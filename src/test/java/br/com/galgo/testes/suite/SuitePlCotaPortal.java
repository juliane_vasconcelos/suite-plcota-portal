package br.com.galgo.testes.suite;

import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.com.galgo.cancelar.portal.CancelarPlCotaPortal;
import br.com.galgo.consultar.portal.ConsultarPlCotaPortal;
import br.com.galgo.enviar.portal.EnviarPlCotaPortal;
import br.com.galgo.reenviar.portal.ReenviarPlCotaPortal;
import br.com.galgo.testes.recursos_comuns.enumerador.config.Ambiente;
import br.com.galgo.testes.recursos_comuns.suite.StopOnFirstFailureSuite;
import br.com.galgo.testes.recursos_comuns.utils.SuiteUtils;

@RunWith(StopOnFirstFailureSuite.class)
@Suite.SuiteClasses({ EnviarPlCotaPortal.class,//
		ConsultarPlCotaPortal.class,//
		CancelarPlCotaPortal.class, //
		ReenviarPlCotaPortal.class,//
		ConsultarPlCotaPortal.class })
public class SuitePlCotaPortal {

	private static final String PASTA_SUITE = "PlCotaPortal";

	@BeforeClass
	public static void setUp() throws Exception {
		SuiteUtils.configurarSuiteDefault(Ambiente.HOMOLOGACAO, PASTA_SUITE);
	}

}
